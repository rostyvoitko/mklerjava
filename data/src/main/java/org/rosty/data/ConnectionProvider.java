package org.rosty.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ConnectionProvider {

    private final static String URL = "jdbc:postgresql://localhost:5432/makler";
    private final static String USERNAME = "admin";
    private final static String PASSWORD = "ichbinsatt";

    private final List<Connection> connections = new ArrayList<>();


    public Connection provide() {
        try {
            Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            connections.add(connection);
            return connection;
        } catch (SQLException e) {
            throw new RuntimeException("Error while connecting to database", e);
        }
    }

    public void closeAll() {
        connections.forEach(connection -> {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new RuntimeException("Error while closing connection", e);
            }
        });
    }
}
