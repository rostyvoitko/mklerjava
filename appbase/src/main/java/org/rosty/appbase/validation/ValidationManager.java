package org.rosty.appbase.validation;

import java.util.ArrayList;
import java.util.List;

public class ValidationManager<MT, ET extends Exception> {

    MT modelType;
    ET exceptionType;

    private final List<Validator<MT, ET>> validators;

    public ValidationManager() {
        this.validators = new ArrayList<>();
    }

    public void addValidator(Validator<MT, ET> validator) {
        this.validators.add(validator);
    }

    public void validate(MT model) throws ValidationException {
        for (Validator<MT, ET> validator : this.validators) {
            try {
                validator.validate(model);
            } catch (Exception e) {
                Exception exception = (ET) e;
                throw new ValidationException("Exception happened during validation : ", exception);
            }

        }
    }
}
