package org.rosty.appbase.validation;

public interface Validator<MT, ET extends Exception> {

    void validate(MT model) throws ET;
}
