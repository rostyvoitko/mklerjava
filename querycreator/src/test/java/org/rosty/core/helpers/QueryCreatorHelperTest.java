package org.rosty.core.helpers;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.rosty.core.AddressTestTable;
import org.rosty.core.meta.FieldData;
import org.rosty.core.meta.ManyToOne;
import org.rosty.core.meta.PrimaryKey;
import org.rosty.core.enums.PostgreSQLDataType;
import org.rosty.core.exceptions.QueryCreationException;

@ExtendWith(MockitoExtension.class)
class QueryCreatorHelperTest {

    @InjectMocks
    @Spy
    private QueryCreatorHelper classUnderTest;

    @Test
    void createColumnsQueryString() throws QueryCreationException {
        // Arrange
        FieldData fieldData1 = new PrimaryKey("id", PostgreSQLDataType.BIGINT, Long.class);
        FieldData fieldData2 = new FieldData("name", PostgreSQLDataType.VARCHAR, String.class);
        FieldData fieldData3 = new FieldData("surname", PostgreSQLDataType.VARCHAR, String.class);
        FieldData fieldData4 = new FieldData("email", PostgreSQLDataType.VARCHAR, String.class);
        FieldData fieldData5 = new ManyToOne("address", PostgreSQLDataType.BIGINT, Long.class, new AddressTestTable(), "id");
        FieldData[] fieldData = new FieldData[] {fieldData1, fieldData2, fieldData3, fieldData4, fieldData5};
        String[] columns = new String[] {"id", "name", "surname", "email", "address"};
        String expected = "(id SERIAL PRIMARY KEY, name varchar(255), surname varchar(255), email varchar(255), address bigint REFERENCES address(id))";

        // Act
        classUnderTest.createColumnsQueryString(fieldData);
        // Assert
        assertEquals(expected, classUnderTest.createColumnsQueryString(fieldData));
    }

    @Test
    void createColumnsWithoutTypes() {
        // Arrange
        FieldData fieldData1 = new PrimaryKey("id", PostgreSQLDataType.BIGINT, Long.class);
        FieldData fieldData2 = new FieldData("name", PostgreSQLDataType.VARCHAR, String.class);
        FieldData fieldData3 = new FieldData("surname", PostgreSQLDataType.VARCHAR, String.class);
        FieldData fieldData4 = new ManyToOne("email", PostgreSQLDataType.VARCHAR, String.class, new AddressTestTable(), "id");
        String expected = "(name, surname, email)";
        // Act
        String result = classUnderTest.createColumnsWithoutTypes(new FieldData[] {fieldData1, fieldData2, fieldData3, fieldData4});
        // Assert
        assertEquals(expected, result);
    }

    @Test
    void createValuesPlaceholders() {
       // Arrange

        String expected = "(??, ??, ??)";

        // Act
        String result = classUnderTest.createValuesPlaceholders(3);

        // Assert
        assertEquals(expected, result);
    }

    @Test
    void createSelectColumns() {
        // Arrange
        FieldData fieldData1 = new PrimaryKey("id", PostgreSQLDataType.BIGINT, Long.class);
        FieldData fieldData2 = new FieldData("name", PostgreSQLDataType.VARCHAR, String.class);
        FieldData fieldData3 = new FieldData("surname", PostgreSQLDataType.VARCHAR, String.class);
        FieldData fieldData4 = new FieldData("age", PostgreSQLDataType.INTEGER, Integer.class);

        FieldData[] fieldsData = new FieldData[] {fieldData1, fieldData2, fieldData3, fieldData4};

        String expected = "profile.id, profile.name, profile.surname, profile.age";
        // Act
        String result = classUnderTest.createSelectColumns(fieldsData, "profile");
        // Assert
        assertEquals(expected, result);
    }
}