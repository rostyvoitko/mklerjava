package org.rosty.core;

import org.rosty.core.base.DBModel;
import org.rosty.core.base.DBTable;

public class AddressTestModel extends DBModel {
    private Integer id;
    private String street;
    private Integer house;
    private Integer flat;
    private String city;

    public AddressTestModel(Integer id, String street, Integer house, Integer flat, String city) {
        this.id = id;
        this.street = street;
        this.house = house;
        this.flat = flat;
        this.city = city;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getHouse() {
        return house;
    }

    public void setHouse(Integer house) {
        this.house = house;
    }

    public Integer getFlat() {
        return flat;
    }

    public void setFlat(Integer flat) {
        this.flat = flat;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public DBTable getDBMetaData() {
        return new AddressTestTable();
    }

    @Override
    public Object[] getAllValues() {
        return new Object[] {id, street, house, flat, city};
    }
}
