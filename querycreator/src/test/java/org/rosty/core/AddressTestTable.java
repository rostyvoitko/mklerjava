package org.rosty.core;

import org.rosty.core.enums.PostgreSQLDataType;
import org.rosty.core.meta.FieldData;
import org.rosty.core.base.DBTable;
import org.rosty.core.meta.PrimaryKey;

public class AddressTestTable implements DBTable {

    private PrimaryKey id = new PrimaryKey("id", PostgreSQLDataType.INTEGER, Integer.class);
    private FieldData street = new FieldData("street", PostgreSQLDataType.VARCHAR, String.class);
    private FieldData house = new FieldData("house", PostgreSQLDataType.INTEGER, Integer.class);
    private FieldData flat = new FieldData("flat", PostgreSQLDataType.INTEGER, Integer.class);
    private FieldData city = new FieldData("city", PostgreSQLDataType.VARCHAR, String.class);

    @Override
    public String getTableName() {
        return "address";
    }

    @Override
    public FieldData[] getMetaData() {
        return new FieldData[] {id, street, house, flat, city};
    }
}
