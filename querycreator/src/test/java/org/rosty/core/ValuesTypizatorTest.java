package org.rosty.core;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.rosty.core.enums.PostgreSQLDataType;
import org.rosty.core.meta.FieldData;
import org.rosty.core.meta.ManyToOne;
import org.rosty.core.meta.PrimaryKey;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class ValuesTypizatorTest {


    @InjectMocks
    private ValuesTypizator classUnderTest;

    @Test
    void getValueData() {
        // Arrange
        PrimaryKey fieldData1 = new PrimaryKey("id", PostgreSQLDataType.BIGINT, Long.class);
        FieldData fieldData2 = new FieldData("name", PostgreSQLDataType.VARCHAR, String.class);
        FieldData fieldData3 = new FieldData("surname", PostgreSQLDataType.VARCHAR, String.class);
        FieldData fieldData4 = new FieldData("age", PostgreSQLDataType.INTEGER, Integer.class);
        FieldData fieldData5 = new FieldData("money", PostgreSQLDataType.REAL, Double.class);
        ManyToOne fieldData6 = new ManyToOne("address", PostgreSQLDataType.BIGINT, Long.class, new AddressTestTable(), "id");

        FieldData[] fields = new FieldData[] {fieldData1, fieldData2, fieldData3, fieldData4, fieldData5, fieldData6};

        Object value1 = 1L;
        Object value2 = "John";
        Object value3 = "Doe";
        Object value4 = 25;
        Object value5 = 100.0;
        Object value6 = 1L;

        Object[] values = new Object[] {value1, value2, value3, value4, value5, value6};

        // Act
        ValueData[] result = classUnderTest.getValueData(values, fields);

        // Assert
        assertEquals(6, result.length);
        assertEquals(1L, result[0].getValue());
        assertTrue(result[0].isPrimaryKey());
        assertEquals("John", result[1].getValue());
        assertEquals("Doe", result[2].getValue());
        assertEquals(25, result[3].getValue());
        assertEquals(100.0, result[4].getValue());
        assertEquals(1L, result[5].getValue());
    }
}