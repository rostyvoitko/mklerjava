package org.rosty.creator;

import org.junit.jupiter.api.Test;
import org.rosty.core.AddressTestModel;
import org.rosty.core.AddressTestTable;
import org.rosty.core.enums.QueryType;
import org.rosty.core.meta.QueryObj;

import static org.junit.jupiter.api.Assertions.assertEquals;

class QueryCreatorImplTest {

    @Test
    void buildQuery() {
    }

    @Test
    void createInsertQuery() {
    }

    @Test
    void createUpdateQuery() {
    }

    @Test
    void createDeleteQuery() {
    }

    @Test
    void createSelectQuery() {
        // Arrange
        QueryCreatorImpl<AddressTestModel> queryCreator = new QueryCreatorImpl<>(AddressTestModel.class, new AddressTestTable());
        QueryObj<AddressTestModel> queryObj = new QueryObj<>(QueryType.SELECT);
        queryObj.setId(1L);
        queryObj.setPrimaryKeyColumnName("id");
        String expected = "SELECT address.id, address.street, address.house, address.flat, address.city FROM address WHERE address.id = 1";

        // Act
        String query = queryCreator.createSelectQuery(queryObj);
        // Assert
        assertEquals(expected, query);
    }

    @Test
    void createSelectAllQuery() {
    }

    @Test
    void createCreateTableQuery() {
    }

    @Test
    void createDropTableQuery() {
    }
}