module querycreator {
    exports org.rosty.creator;
    exports org.rosty.core;
    exports org.rosty.core.base;
    exports org.rosty.core.enums;
    exports org.rosty.core.exceptions;
    exports org.rosty.core.helpers;
    exports org.rosty.core.meta;
    requires org.rosty.appbase;
}