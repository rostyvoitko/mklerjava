package org.rosty.creator;

import org.rosty.core.meta.FieldData;
import org.rosty.core.meta.QueryObj;
import org.rosty.core.base.DBModel;
import org.rosty.core.base.DBTable;
import org.rosty.core.exceptions.QueryCreationException;
import org.rosty.core.helpers.QueryCreatorHelper;

public class QueryCreatorImpl<T extends DBModel> {

    Class<T> modelClass;

    T modelType;

    private final DBTable tableData;

    private final FieldData[] metaData;

    private final QueryCreatorHelper helper;

    public QueryCreatorImpl(Class<T> modelClass, DBTable tableData) {
        this.modelClass = modelClass;
        this.tableData = tableData;
        this.metaData = tableData.getMetaData();
        this.helper = new QueryCreatorHelper();
    }

    public String buildQuery(QueryObj<T> queryObj) throws QueryCreationException {
        String query = "";
        switch(queryObj.getQueryType()) {
            case INSERT:
                query = createInsertQuery(queryObj);
                break;
            case UPDATE:
                query = createUpdateQuery(queryObj);
                break;
            case DELETE:
                query = createDeleteQuery(queryObj);
                break;
            case SELECT:
                query = createSelectQuery(queryObj);
                break;
            case SELECT_ALL:
                query = createSelectAllQuery();
                break;
            case CREATE_TABLE:
                query = createCreateTableQuery(tableData);
                break;
            case DROP_TABLE:
                query = createDropTableQuery();
                break;
            default:
                throw new QueryCreationException("Error happened while creating query object");
        }
        return query;
    }

    /**
     * Creates insert query for given model
     * @param queryObj - {@link QueryObj} object
     * @return - insert query
     */
    public String createInsertQuery(QueryObj<T> queryObj) {
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO ");
        query.append(tableData.getTableName());
        query.append(" ");
        query.append(helper.createColumnsWithoutTypes(metaData));
        query.append(" VALUES ");
        query.append(helper.createValuesPlaceholders(metaData.length));
        return query.toString();
    }

    public String createUpdateQuery(QueryObj<T> queryObj) {
        return null;
    }

    public String createDeleteQuery(QueryObj<T> queryObj) {
        return null;
    }


    public String createSelectQuery(QueryObj<T> queryObj) {
        StringBuilder query = new StringBuilder();
        query.append(createSelectAllQuery());
        query.append(" WHERE ");
        query.append(tableData.getTableName());
        query.append(".");
        query.append(queryObj.getPrimaryKeyColumnName());
        query.append(" = ");
        query.append(queryObj.getId());
        return query.toString();
    }

    public String createSelectAllQuery() {
        StringBuilder query = new StringBuilder();
        query.append("SELECT ");
        query.append(helper.createSelectColumns(tableData.getMetaData(), tableData.getTableName()));
        query.append(" FROM ");
        query.append(tableData.getTableName());
        return query.toString();
    }

    public String createCreateTableQuery(DBTable tableData) throws QueryCreationException {
        StringBuilder query = new StringBuilder();
        FieldData[] fields = tableData.getMetaData();
        query.append("CREATE TABLE IF NOT EXISTS ");
        query.append(tableData.getTableName());
        query.append(" ");
        query.append(helper.createColumnsQueryString(fields));
        query.append(";");
        return query.toString();
    }

    public String createDropTableQuery() {
        return null;
    }
}
