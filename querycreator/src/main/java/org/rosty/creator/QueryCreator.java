package org.rosty.creator;

import org.rosty.core.base.DBModel;

public interface QueryCreator {
    String createInsertQuery(DBModel model);
    String createUpdateQuery(DBModel model);
    String createDeleteQuery(Long id);
    String createSelectQuery(Long id);
    String createSelectAllQuery();
    String createCreateTableQuery();
    String createDropTableQuery();
}
