package org.rosty.core;

/**
 * Class representing a value and its type for creating and sql query.
 */
public class ValueData {

    private Class<?> type;
    private Object value;
    private boolean isPrimaryKey = false;

    public ValueData(Class<?> type, Object value) {
        this.type = type;
        this.value = value;
    }

    public ValueData(Class<?> type, Object value, boolean isPrimaryKey) {
        this.type = type;
        this.value = value;
        this.isPrimaryKey = isPrimaryKey;
    }

    public Class<?> getType() {
        return type;
    }

    public Object getValue() {
        return value;
    }

    public boolean isPrimaryKey() {
        return isPrimaryKey;
    }
}
