package org.rosty.core;

import org.rosty.core.meta.FieldData;
import org.rosty.core.meta.PrimaryKey;

public class ValuesTypizator {

    public ValueData[] getValueData(Object[] values, FieldData[] metaData) {
        ValueData[] valueData = new ValueData[values.length];
        for(int i = 0; i < values.length; i++) {
            if(values[i] != null) {
                if(!metaData[i].getJavaType().isAssignableFrom(values[i].getClass())) {
                    throw new IllegalArgumentException("The value " + values[i] + " is not of type " + metaData[i].getJavaType().getName());
                } else {
                    if(metaData[i].isPrimaryKey() || metaData[i] instanceof PrimaryKey) {
                        valueData[i] = new ValueData(metaData[i].getJavaType(), values[i], true);
                    } else {
                        valueData[i] = new ValueData(metaData[i].getJavaType(), values[i]);
                    }
                }
            }
        }
        return valueData;
    }
}
