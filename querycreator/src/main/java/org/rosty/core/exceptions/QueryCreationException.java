package org.rosty.core.exceptions;

/**
 * Exception to be thrown when a query cannot be created.
 */
public class QueryCreationException extends Exception {

    /**
     * @param message - the message to be displayed when the exception is thrown.
     */
    public QueryCreationException(String message) {
        super(message);
    }
}
