package org.rosty.core.enums;

public enum QueryType {
    INSERT,
    UPDATE,
    DELETE,
    SELECT,
    SELECT_ALL,
    CREATE_TABLE,
    DROP_TABLE,
}
