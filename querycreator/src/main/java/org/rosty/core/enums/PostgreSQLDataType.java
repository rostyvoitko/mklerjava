package org.rosty.core.enums;

import org.rosty.core.base.SQLDataType;

/**
 * Class for SQL data types
 */
public enum PostgreSQLDataType implements SQLDataType {
    INTEGER("integer"),
    REAL("real"),
    BOOLEAN("boolean"),
    BIGINT("bigint"),
    TEXT("text"),
    VARCHAR("varchar"),
    TIMESTAMP("timestamp"),
    DATE("date");

    private final String representation;

    PostgreSQLDataType(String representation) {
        this.representation = representation;
    }

    public String getRepresentation() {
        return representation;
    }
}
