package org.rosty.core.validator;

import org.rosty.appbase.validation.Validator;
import org.rosty.core.Functionator;
import org.rosty.core.exceptions.QueryCreationException;

public class PrimaryKeyValidatorImpl implements Validator<Functionator.PrimaryKeyValidationData, QueryCreationException> {

    @Override
    public void validate(Functionator.PrimaryKeyValidationData data) throws QueryCreationException {
        int primaryKeyCount = data.primaryKeyCount();
        String primaryKeyCol = data.primaryKeyColumnName();
        if (primaryKeyCount > 1) {
            throw new QueryCreationException("multiple primary keys is not supported yet");
        } else if (primaryKeyCount == 0) {
            throw new QueryCreationException("no primary key found");
        } else if (primaryKeyCol == null || primaryKeyCol.isEmpty()) {
            throw new QueryCreationException("primary key is null or empty");
        }
    }
}
