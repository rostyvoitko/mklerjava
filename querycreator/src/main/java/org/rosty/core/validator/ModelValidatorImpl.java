package org.rosty.core.validator;


import org.rosty.appbase.validation.Validator;
import org.rosty.core.base.DBModel;
import org.rosty.core.exceptions.QueryCreationException;

public class ModelValidatorImpl implements Validator<DBModel, QueryCreationException> {

    @Override
    public void validate(DBModel dbModel) throws QueryCreationException {
        if (dbModel == null) {
            throw new QueryCreationException("dbModel is null");
        } else if (dbModel.getAllValues() == null || dbModel.getDBMetaData().getMetaData() == null) {
            throw new QueryCreationException("dbModel is null");
        } else if (dbModel.getAllValues().length == 0 || dbModel.getDBMetaData().getMetaData().length == 0) {
            throw new QueryCreationException("provide values in dbModel.getDBMetaData() and dbModel.getAllValues() respectively");
        }
        if (dbModel.getDBMetaData().getMetaData().length != dbModel.getAllValues().length) {
            throw new QueryCreationException("columns and values length mismatch, your dbModel is not valid");
        }
    }
}
