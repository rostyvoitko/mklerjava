package org.rosty.core.base;

import org.rosty.core.meta.FieldData;

/**
 * Metadata for a database table.
 */
public interface DBTable {
    String getTableName();
    FieldData[] getMetaData();
}
