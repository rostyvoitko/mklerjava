package org.rosty.core.base;

public interface SQLDataType {
    String getRepresentation();
}
