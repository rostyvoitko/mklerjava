package org.rosty.core.base;

/**
 * Base class for all DBModels.
 */
public abstract class DBModel {

    public abstract DBTable getDBMetaData();

    public abstract Object[] getAllValues();
}
