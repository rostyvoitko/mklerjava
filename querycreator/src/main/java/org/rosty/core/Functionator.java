package org.rosty.core;

import org.rosty.core.base.DBModel;
import org.rosty.core.meta.PrimaryKey;

import java.util.function.Function;

public class Functionator {

    public static Function<DBModel, PrimaryKeyValidationData> dbModelPrimaryKeyValidationDataFunction() {
        return (dbModel) -> {
            String primaryKeyColumnName = null;
            int primaryKeyCount = 0;
            for (int i = 0; i < dbModel.getDBMetaData().getMetaData().length; i++) {
                if (dbModel.getDBMetaData().getMetaData()[i].isPrimaryKey() || dbModel.getDBMetaData().getMetaData()[i] instanceof PrimaryKey) {
                    primaryKeyColumnName = dbModel.getDBMetaData().getMetaData()[i].getColumnName();
                    primaryKeyCount++;
                }
            }
            return new PrimaryKeyValidationData(primaryKeyColumnName, primaryKeyCount);
        };
    }

    public record PrimaryKeyValidationData(String primaryKeyColumnName, int primaryKeyCount) {
    }

}
