package org.rosty.core;

import org.rosty.appbase.validation.ValidationException;
import org.rosty.appbase.validation.ValidationManager;
import org.rosty.core.base.DBModel;
import org.rosty.core.enums.QueryType;
import org.rosty.core.exceptions.QueryCreationException;
import org.rosty.core.meta.QueryObj;
import org.rosty.core.validator.ModelValidatorImpl;
import org.rosty.core.validator.PrimaryKeyValidatorImpl;

/**
 * Middle layer between the DBModel and the QueryObj.
 * This class is responsible for creating a QueryObj for a given DBModel.
 */
public class QueryObjCreator {

    private final ValuesTypizator valuesTypizator;
    private final ModelValidatorImpl modelValidator;
    private final PrimaryKeyValidatorImpl primaryKeyValidator;

    public QueryObjCreator(ValuesTypizator vt) {
        valuesTypizator = vt;
        modelValidator = new ModelValidatorImpl();
        primaryKeyValidator = new PrimaryKeyValidatorImpl();
    }

    /**
     * prepares a query object for the given dbModel
     * @param queryType - {@link QueryType} enum
     * @param dbModel - {@link DBModel} object
     * @return - {@link QueryObj} object
     * @param <T> - {@link DBModel} object
     */
    public <T extends DBModel> QueryObj<T> createQueryObj(QueryType queryType, T dbModel) throws ValidationException {
        Functionator.PrimaryKeyValidationData pkData = Functionator.dbModelPrimaryKeyValidationDataFunction().apply(dbModel);

        ValidationManager<DBModel, QueryCreationException> dbModelValidationManager = new ValidationManager<>();
        dbModelValidationManager.addValidator(modelValidator);
        ValidationManager<Functionator.PrimaryKeyValidationData, QueryCreationException> primaryKeyValidationManager = new ValidationManager<>();
        primaryKeyValidationManager.addValidator(primaryKeyValidator);

        dbModelValidationManager.validate(dbModel);
        primaryKeyValidationManager.validate(pkData);

        QueryObj<T> queryObj = new QueryObj<>(queryType);

        Object[] values = dbModel.getAllValues();
        ValueData[] valueData = valuesTypizator.getValueData(values, dbModel.getDBMetaData().getMetaData());
        queryObj.setValueData(valueData);
        queryObj.setPrimaryKeyColumnName(pkData.primaryKeyColumnName());
        if (queryType == QueryType.UPDATE || queryType == QueryType.DELETE || queryType == QueryType.SELECT) {
            Object id = extractId(valueData);
            queryObj.setId(id);
        }
        return queryObj;
    }

    private <T extends DBModel> Object extractId(ValueData[] valueData) {
        for (ValueData vd : valueData) {
            if (vd.isPrimaryKey()) {
                return vd.getValue();
            }
        }
        return null;
    }

}
