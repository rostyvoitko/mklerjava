package org.rosty.core.meta;

import org.rosty.core.ValueData;
import org.rosty.core.base.DBModel;
import org.rosty.core.enums.QueryType;

/**
 * Class to be used for creating and ending queries
 * @param <T> - {@link DBModel} object type
 */
public class QueryObj<T extends DBModel> {

    Class<T> dbModelClass;
    private QueryType queryType;
    private ValueData[] valueData;

    private String primaryKeyColumnName;

    private Object id;

    /**
     * @param queryType - {@link QueryType} enum
     */
    public QueryObj(QueryType queryType) {
        this.queryType = queryType;
    }

    public String getPrimaryKeyColumnName() {
        return primaryKeyColumnName;
    }

    public void setPrimaryKeyColumnName(String primaryKeyColumnName) {
        this.primaryKeyColumnName = primaryKeyColumnName;
    }

    public QueryType getQueryType() {
        return queryType;
    }

    public Class<T> getDbModelClass() {
        return dbModelClass;
    }

    public ValueData[] getValueData() {
        return valueData;
    }

    public void setValueData(ValueData[] valueData) {
        this.valueData = valueData;
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }
}
