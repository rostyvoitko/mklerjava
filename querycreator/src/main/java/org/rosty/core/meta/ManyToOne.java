package org.rosty.core.meta;

import org.rosty.core.base.DBTable;
import org.rosty.core.base.SQLDataType;

public class ManyToOne extends FieldData {

    private DBTable associated;

    private String associatedColumn = "id";

    /**
     * @param columnName
     * @param dataType
     * @param javaType
     */
    public ManyToOne(String columnName, SQLDataType dataType, Class<?> javaType, DBTable associated) {
        super(columnName, dataType, javaType);
        this.associated = associated;
    }

    public ManyToOne(String columnName, SQLDataType dataType, Class<?> javaType, DBTable associated, String associatedColumn) {
        super(columnName, dataType, javaType);
        this.associated = associated;
        this.associatedColumn = associatedColumn;
    }

    public DBTable getAssociated() {
        return associated;
    }

    public String getAssociatedColumn() {
        return associatedColumn;
    }
}
