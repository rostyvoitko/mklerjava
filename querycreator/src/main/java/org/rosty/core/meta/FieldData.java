package org.rosty.core.meta;

import org.rosty.core.base.SQLDataType;

/**
 * This class is used to store the data of a field for a database table.
 */
public class FieldData {

    private String columnName;
    private SQLDataType dataType;
    private Class<?> javaType;
    private boolean isPrimaryKey = false;
    private boolean isAutoIncrement;
    private boolean isNullable = false;
    private boolean isUnique = false;
    private int length = 255;

    /**
     * Standard constructor.
     * Primary key is set to false by default.
     * IsNullable is set to false by default.
     * IsUnique is set to false by default.
     * @param columnName - The name of the column in the database.
     * @param dataType - The SQLDataType of the column.
     * @param javaType - The Java type of the column.
     */
    public FieldData(String columnName, SQLDataType dataType, Class<?> javaType) {
        this.columnName = columnName;
        this.dataType = dataType;
        this.javaType = javaType;
    }

    /**
     * Only use this constructor if you want to set the primary key yourself.
     * @param columnName - The name of the column in the database.
     * @param dataType - The SQLDataType of the column.
     * @param javaType - The Java type of the column.
     * @param isPrimaryKey - Set to true if this is the primary key.
     * @param isAutoIncrement - Set to true if this is the primary key and it is auto incremented.
     */
    public FieldData(String columnName, SQLDataType dataType, Class<?> javaType, boolean isPrimaryKey, boolean isAutoIncrement) {
        this.columnName = columnName;
        this.dataType = dataType;
        this.javaType = javaType;
        this.isPrimaryKey = isPrimaryKey;
        this.isAutoIncrement = isAutoIncrement;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public SQLDataType getDataType() {
        return dataType;
    }

    public void setDataType(SQLDataType dataType) {
        this.dataType = dataType;
    }

    public Class<?> getJavaType() {
        return javaType;
    }

    public void setJavaType(Class<?> javaType) {
        this.javaType = javaType;
    }

    public boolean isPrimaryKey() {
        return isPrimaryKey;
    }

    public void setPrimaryKey(boolean primaryKey) {
        isPrimaryKey = primaryKey;
    }

    public boolean isAutoIncrement() {
        return isAutoIncrement;
    }

    public void setAutoIncrement(boolean autoIncrement) {
        isAutoIncrement = autoIncrement;
    }

    public boolean isNullable() {
        return isNullable;
    }

    public void setNullable(boolean nullable) {
        isNullable = nullable;
    }

    public boolean isUnique() {
        return isUnique;
    }

    public void setUnique(boolean unique) {
        isUnique = unique;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
