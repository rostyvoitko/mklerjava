package org.rosty.core.meta;

import org.rosty.core.base.DBModel;

public class ManyToMany {

    private String intermediaryTableName;
    private Class<? extends DBModel> associate1;
    private Class<? extends DBModel> associate2;
    private String associateColumnName1;
    private String associateColumnName2;

}
