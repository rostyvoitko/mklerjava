package org.rosty.core.meta;

import org.rosty.core.base.SQLDataType;

public class PrimaryKey extends FieldData {

    /**
     * Is autoincremented by default. Set to false if you want to set the value yourself.
     * @param columnName
     * @param dataType
     * @param javaType
     */
    public PrimaryKey(String columnName, SQLDataType dataType, Class<?> javaType) {
        super(columnName, dataType, javaType);
        super.setPrimaryKey(true);
        super.setAutoIncrement(true);
    }

    @Override
    public void setAutoIncrement(boolean autoIncrement) {
        super.setAutoIncrement(autoIncrement);
    }
}
