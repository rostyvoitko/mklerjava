package org.rosty.core.helpers;

import static java.util.Arrays.stream;
import static org.rosty.core.enums.PostgreSQLDataType.VARCHAR;

import java.util.stream.Collectors;

import org.rosty.core.meta.FieldData;
import org.rosty.core.meta.ManyToOne;
import org.rosty.core.meta.PrimaryKey;
import org.rosty.core.exceptions.QueryCreationException;

/**
 * Helper class for QueryCreator.
 */
public class QueryCreatorHelper {

    public QueryCreatorHelper() {
    }

    public String createColumnsQueryString(FieldData[] fieldData) throws QueryCreationException {
        StringBuilder query = new StringBuilder();
        String[] columnDefinitions = new String[fieldData.length];
        for (int i = 0; i < fieldData.length; i++) {
            String columnDefinition = "";
            columnDefinition += fieldData[i].getColumnName();
            // 1. if column is primary key, add SERIAL PRIMARY KEY
            if (fieldData[i].isPrimaryKey()) {
                columnDefinition += " SERIAL PRIMARY KEY";
            } else {
                // 2. else add column data type
                columnDefinition += " ";
                columnDefinition += fieldData[i].getDataType().getRepresentation();
                // 2.1 if data type is VARCHAR, add length
                if(fieldData[i].getDataType() == VARCHAR) {
                    int length = fieldData[i].getLength();
                    columnDefinition += "(" + length + ")";
                }
            }

            try {
                // 3. handle foreign keys
                if(fieldData[i] instanceof ManyToOne manyToOne) {
                    columnDefinition += " REFERENCES " + manyToOne.getAssociated().getTableName() + "(" + manyToOne.getAssociatedColumn() + ")";
                }
            } catch (NullPointerException npl) {
                throw new QueryCreationException("The column " + fieldData[i].getColumnName() + " is a foreign key but the associated column is not set.");
            }

            // 4. add column definition to array
            columnDefinitions[i] = columnDefinition;
        }
        query.append(stream(columnDefinitions).collect(Collectors.joining(", ", "(", ")")));
        return query.toString();
    }

    public String createColumnsWithoutTypes(FieldData[] fieldData) {
        return stream(fieldData).filter(field -> !(field instanceof PrimaryKey))
                .map(FieldData::getColumnName)
                .collect(Collectors.joining(", ", "(", ")"));
    }

    public String createValuesPlaceholders(int numberOfColumns) {
        String query = "(";
        for (int i = 0; i < numberOfColumns; i++) {
            query += "??, ";
        }
        query = query.substring(0, query.length() - 2);
        query += ")";
        return query;
    }

    public String createSelectColumns(FieldData[] metaData, String tableName) {
        return stream(metaData)
                .map(FieldData::getColumnName)
                .map(columnName -> tableName + "." + columnName)
                .collect(Collectors.joining(", "));
    }
}
