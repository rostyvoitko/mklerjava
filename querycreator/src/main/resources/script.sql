CREATE TABLE IF NOT EXISTS profile (
                                       id SERIAL PRIMARY KEY,
                                       first_name VARCHAR(255),
                                       last_name VARCHAR(255),
                                       phone_number VARCHAR(255),
                                       created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP);

CREATE TABLE IF NOT EXISTS address (
                                       id SERIAL PRIMARY KEY,
                                       street VARCHAR(255),
                                       street_number VARCHAR(255),
                                       city VARCHAR(255),
                                       zip VARCHAR(255),
                                       created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS OWNER (
                                     id SERIAL PRIMARY KEY ,
                                     profile_id INTEGER,
                                     FOREIGN KEY (profile_id) REFERENCES profile(id) on delete cascade
);

CREATE TABLE IF NOT EXISTS BROKER (
                                      id SERIAL PRIMARY KEY ,
                                      profile_id INTEGER,
                                      FOREIGN KEY (profile_id) REFERENCES profile(id) on delete cascade
);

CREATE TABLE IF NOT EXISTS INTERESTED (
                                          id SERIAL PRIMARY KEY ,
                                          profile_id INTEGER,
                                          address_id INTEGER,
                                          regular BOOLEAN,
                                          FOREIGN KEY (profile_id) REFERENCES profile(id) on delete cascade,
                                          FOREIGN KEY (address_id) REFERENCES address(id)
);

CREATE TABLE IF NOT EXISTS PROPERTY_TYPE (
                                             id SERIAL PRIMARY KEY ,
                                             name VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS PROPERTY (
                                        id SERIAL PRIMARY KEY ,
                                        address_id INTEGER,
                                        owner_id INTEGER,
                                        broker_id INTEGER,
                                        price INTEGER,
                                        area REAL,
                                        FOREIGN KEY (address_id) REFERENCES address(id) on delete cascade,
                                        FOREIGN KEY (owner_id) REFERENCES owner(id) on delete cascade,
                                        FOREIGN KEY (broker_id) REFERENCES broker(id) on delete cascade
);


CREATE TABLE IF NOT EXISTS PROPERTY_PROPERTY_TYPE (
                                                      id SERIAL PRIMARY KEY ,
                                                      property_id INTEGER,
                                                      property_type_id INTEGER,
                                                      FOREIGN KEY (property_id) REFERENCES property(id) on delete cascade,
                                                      FOREIGN KEY (property_type_id) REFERENCES property_type(id) on delete cascade
);

CREATE TABLE IF NOT EXISTS APPOINTMENT (
                                           id SERIAL PRIMARY KEY ,
                                           property_id INTEGER,
                                           interested_id INTEGER,
                                           date DATE,
                                           time TIME
);

CREATE TABLE IF NOT EXISTS BID (
                                   id SERIAL PRIMARY KEY ,
                                   property_id INTEGER,
                                   interested_id INTEGER,
                                   bid INTEGER,
                                   isDeal BOOLEAN
);

# add profile
INSERT INTO profile (first_name, last_name, phone_number) VALUES('Moinhard', 'Moinmann', '01234567890');
INSERT INTO profile (first_name, last_name, phone_number) VALUES('Moischa', 'Mointen', '09994567890');
INSERT INTO profile (first_name, last_name, phone_number) VALUES('Moinika', 'Moinstosik', '02224567890');
INSERT INTO profile (first_name, last_name, phone_number) VALUES('Mointen', 'Moinaber', '08884567890');
INSERT INTO profile (first_name, last_name, phone_number) VALUES('Moi', 'Mo In', '07774567890');
INSERT INTO profile (first_name, last_name, phone_number) VALUES('Moisten', 'Moininchik', '06664567890');
INSERT INTO profile (first_name, last_name, phone_number) VALUES('Moin', 'Moinen', '05554567890');
INSERT INTO profile (first_name, last_name, phone_number) VALUES('Mo', 'In', '03334567890');
INSERT INTO profile (first_name, last_name, phone_number) VALUES('Mooi', 'Moinser', '01114567890');

# add address
INSERT INTO address (street, street_number, city, zip) VALUES ('Moinstreet 88', 'Moinberg', 'New Moinlean', '01488');
INSERT INTO address (street, street_number, city, zip) VALUES('Moinweg 88', 'Moinow', 'New Moinlean', '01414');
INSERT INTO address (street, street_number, city, zip) VALUES('Moinkampfallee 88', 'Mointown', 'New Moinlean', '08814');
INSERT INTO address (street, street_number, city, zip) VALUES('Moinardstreet 88', 'Moinland', 'New Moinlean', '08888');
INSERT INTO address (street, street_number, city, zip) VALUES('Moiner Freicheit street 88', 'Moindeich', 'New Moinlean', '14880');
INSERT INTO address (street, street_number, city, zip) VALUES('Mostreet 88', 'Moinburg', 'New Moinlean', '11488');

# add owner
INSERT INTO owner (profile_id) VALUES (1);
INSERT INTO owner (profile_id) VALUES (2);
INSERT INTO owner (profile_id) VALUES (3);

# add broker
INSERT INTO broker (profile_id) VALUES (4);
INSERT INTO broker (profile_id) VALUES (5);
INSERT INTO broker (profile_id) VALUES (6);

# add interested
INSERT INTO interested (profile_id, address_id, regular) VALUES (7, 1, false),
                                                                (8, 2, true),
                                                                (9, 3, false);

# add property type
INSERT INTO property_type (name) VALUES ('House'),
                                        ('Apartment'),
                                        ('Condo');

# add property
INSERT INTO property (address_id, owner_id, broker_id, price, area) VALUES (1, 1, 1, 100000, 100.3),
                                                                           (2, 2, 2, 200000, 82.5),
                                                                           (3, 3, 3, 300000, 50.8);

# add property property type
INSERT INTO property_property_type (property_id, property_type_id) VALUES (1, 1),
                                                                          (2, 2),
                                                                          (3, 3);

# add appointment
INSERT INTO appointment (property_id, interested_id, date, time) VALUES (1, 1, '2020-01-01', '10:00'),
                                                                        (2, 2, '2020-01-02', '11:00'),
                                                                        (3, 3, '2020-01-03', '12:00');

# add bid
INSERT INTO bid (property_id, interested_id, bid, isDeal) VALUES (1, 1, 100000, true),
                                                                 (2, 2, 200000, false),
                                                                 (3, 3, 300000, true);




